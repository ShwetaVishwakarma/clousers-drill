module.exports.counterFactory = () => {
    let counter = 0;
    function increment() {
        return ++counter;
    }
    function decrement() {
        return --counter;
    }
    return {
        a:increment,
        b:decrement
    }
}

