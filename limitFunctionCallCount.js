module.exports.limitFunctionCallCount = (cb, n) => {
    return function () {
        if (n > 0) {
            n--;
            cb() ;
        }
        else {
            return console.log(null);
        }
    }
}