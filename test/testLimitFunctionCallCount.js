const { limitFunctionCallCount } = require("../limitFunctionCallCount");
function cb() {
    console.log("invoke cb")
}
const invokeFun = limitFunctionCallCount(cb, 2)
invokeFun();
invokeFun();
invokeFun();

