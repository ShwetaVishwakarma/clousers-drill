module.exports.cacheFunction = (cb) => {
    let cache = {};
    return function (val) {
        //console.log(val)
        //console.log(cache)
        if (cache.hasOwnProperty(val)) {
            console.log("It has already seen")
            return cache[val]
        } else {
            cache[val] = cb(val)
            return cache[val]
        }
    }
}